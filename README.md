# foobot
A simple chatbot intended for use on livecoding.tv chat rooms.
The launch script is configured to connect to Rauko's channel,
so if you want to use foobot for your own channel, you should
edit the launch script to have the livecoding credentials of
your (or your bot's) account. You may also want to change which
plugin your bot is using.

The design for foobot ended up being very similar to Errbot,
although I only found out about Errbot after starting work on
foobot. The FooBot class wraps a SleekXMPP client, and has
the notion of plugins. See the footbot/plugin.py for examples
on how to write your own plugins.
