"""A collection of basic plugins for FooBot"""

import os
import random

from .foobot import FooPlug, foocmd, foocmd_admin, foocmd_mention

class EightBall(FooPlug):
    """Adds a magic 8 ball to FooBot"""

    responses = [
        'It is certain',
        'It is decidedly so',
        'Without a doubt',
        'Yes, definitely',
        'You may rely on it',
        'As I see it, yes',
        'Most likely',
        'Outlook good',
        'Yes',
        'Signs point to yes',
        'Reply hazy try again',
        'Ask again later',
        'Better not tell you now',
        'Cannot predict now',
        'Concentrate and ask again',
        'Don\'t count on it',
        'My reply is no',
        'My sources say no',
        'Outlook not so good',
        'Very doubtful',
    ]

    def __init__(self, responses=None):
        if responses:
            self.responses = responses

    @foocmd
    def _8ball(self, _nick, args):
        """Returns a random response if there is a question"""
        if args:
            return random.choice(self.responses)
        else:
            return 'try asking a question'


class Raffle(FooPlug):
    """Adds a raffle to the FooBot"""

    def __init__(self):
        self.entrants = None

    @foocmd_admin
    def raffle_start(self, _nick, _args):
        """Starts (or restarts) the raffle"""
        self.entrants = set()
        return 'raffle started - type `!raffle` to enter'

    @foocmd
    def raffle(self, nick, _args):
        """Enters a sender into the raffle if one is running"""
        if self.entrants is not None:
            self.entrants.add(nick)
        else:
            return 'no raffle to enter'

    @foocmd_admin
    def raffle_winner(self, _nick, _args):
        """Selects a winner of the raffle and ends the raffle"""
        if self.entrants:
            winner = random.choice(list(self.entrants))
            self.entrants = None
            return 'the winner is {}'.format(winner)


class Poll(FooPlug):
    """Adds a poll to the FooBot"""

    def __init__(self):
        self.options = None
        self.votes = None

    @foocmd_admin
    def poll_start(self, _nick, args):
        """Starts a new poll with the given options"""
        if args:
            self.options = {opt.strip() for opt in args.split(',')}
        else:
            self.options = None
        self.votes = {}
        return 'poll started - type `!vote <option>` to vote'

    @foocmd
    def vote(self, nick, args):
        """Cast a vote for the sender"""
        if self.votes is not None:
            opt = args.strip()
            if self.options and opt not in self.options:
                return 'invalid option'
            self.votes[nick] = opt
        else:
            return 'no poll to vote in'

    @foocmd_admin
    def poll_result(self, _nick, _args):
        """Ends the poll and gets the results"""
        if self.votes is not None:
            tally = {}
            for opt in self.votes.values():
                tally[opt] = tally.get(opt, 0) + 1
            lines = ['poll results:']
            for opt, count in tally.items():
                lines.append('{} - {}'.format(opt, count))
            self.votes = None
            return '\n'.join(lines)


class Decide(FooPlug):
    """Adds the decide command to the FooBot"""

    def __init__(self, custom_spliter=None):
        self.custom_spliter = custom_spliter

    @foocmd
    def decide(self, _nick, msg):
        """Returns a random option from the given options"""
        if self.custom_spliter and self.custom_spliter in msg:
            options = msg.split(self.custom_spliter)
        else:
            options = msg.split()

        if options:
            return random.choice(options)
        else:
            return 'no options given'


class Rote(FooPlug):
    """Stores a number of rote commands for the FooBot"""

    def __init__(self, filename=None):
        self.filename = filename
        self.commands = {}
        if filename:
            for line in open(filename):
                line = line.strip()
                if line:
                    self.add(None, line.strip())

    @foocmd_admin
    def add(self, _nick, args):
        """Adds a new rote command to the FooBot"""
        cmd = args.split(None, 1)
        if len(cmd) == 2:
            self.register_command(cmd[0], lambda n, a: cmd[1])
            self.commands[cmd[0]] = cmd[1]
            return 'added command {}'.format(cmd[0])

    @foocmd_admin
    def remove(self, _nick, args):
        """Removes a rote command if it exists"""
        if hasattr(self, args):
            delattr(self, args)
            return 'added command {}'.format(args)
        else:
            return 'no such command'

    @foocmd_admin
    def save(self, _nick, args):
        """Saves rote command to file if possible"""
        if not self.filename:
            return 'no save file'
        if args not in self.commands:
            return 'no such command'

        lines = open(self.filename).readlines()
        lines = [line.strip() for line in lines]

        lines.append('{} {}\n'.format(args, self.commands[args]))
        cmdfile = open(self.filename, 'w')
        cmdfile.write('\n'.join(lines))

        return 'command {} saved'.format(args)


class Greeter(FooPlug):
    """Makes FooBot respond to people greeting and saying goodbye to FooBot"""

    greetings = [
        'hey',
        'hello',
        'hi',
        'sup',
        'howdy',
        'hiya',
        'greetings',
    ]
    goodbyes = [
        'goodbye',
        'bye',
        'see you',
        'see ya',
        'later',
        'cya',
        'so long',
    ]

    def __init__(self, greetings=None, goodbyes=None):
        if greetings:
            self.greetings = greetings
        if goodbyes:
            self.goodbyes = goodbyes

    @foocmd_mention
    def be_polite(self, nick, msg):
        """Handles greetings and goodbyes from users"""
        msg = msg.lower()
        for greeting in self.greetings:
            if greeting in msg:
                return '{} {}!'.format(random.choice(self.greetings), nick)
        for goodbye in self.goodbyes:
            if goodbye in msg:
                return '{} {}!'.format(random.choice(self.goodbyes), nick)


class Quotes(FooPlug):
    """Lets FooBot spout out random quotes on command"""

    def __init__(self, *filenames):
        for filename in filenames:
            lines = open(filename).readlines()
            lines = [line.strip() for line in lines]
            lines = [line for line in lines if line]
            self.add_quotes(os.path.basename(filename), lines)

    def add_quotes(self, name, quotes):
        """Adds a set of quotes to the plugin"""
        self.register_command(name, lambda n, a: random.choice(quotes))
