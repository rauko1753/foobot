"""A simple extensible chatbot"""

import time
import logging
import re
import sleekxmpp

class FooBot(object):
    """Wraps a SleekXMPP client to execute FooPlug commands in a chat room"""

    def __init__(self, jid, pw, room):
        self._client = sleekxmpp.ClientXMPP(jid, pw)
        self._room = room
        self._nick = jid[:jid.find('@')]

        logging.debug('Adding event handlers...')
        self._client.add_event_handler('session_start', self._start)
        self._client.add_event_handler('groupchat_message', self._message)

        logging.debug('Registering plugins...')
        self._client.register_plugin('xep_0030') # service discovery
        self._client.register_plugin('xep_0045') # multi-user chat
        self._client.register_plugin('xep_0199') # xmpp ping

        logging.debug('Final setup...')
        self._plugins = []
        self.prefix = '!'
        self.admin = room[:room.find('@')]
        self.timeout = 10
        self._last_timeout = 0
        self.help_cmds = ['help', self._nick]

        logging.info('Init complete!')

    def _start(self, _event):
        """Handles a start session event (which is empty)"""
        logging.debug('Sending presence...')
        self._client.send_presence()
        logging.debug('Joining MUC...')
        self._client.plugin['xep_0045'].joinMUC(self._room,
                                                self._nick,
                                                wait=True)
        logging.info('Started!')

    def _message(self, msg):
        """Handles a group chat message"""
        nick, body = msg['mucnick'], msg['body']
        if nick != self._nick:
            logging.debug("Recieved: (%s) %s", nick, body)
            if body.startswith(self.prefix):
                self._handle_cmd(nick, body)
            elif self._nick in body:
                self._handle_mention(nick, body)

    def _handle_cmd(self, nick, body):
        cmd = re.sub(r'\s\s+', ' ', body[len(self.prefix):]).strip()
        admin = nick == self.admin
        for plugin in self._plugins:
            response = plugin.handle_cmd(nick, cmd, admin)
            if response:
                self._handle_response(response, admin)
                break
        else:
            if cmd in self.help_cmds:
                commands = set()
                for plugin in self._plugins:
                    commands.update(plugin.handle_help())
                self.send('Available commands: ' + ' '.join(sorted(commands)))

    def _handle_mention(self, nick, body):
        for plugin in self._plugins:
            response = plugin.handle_mention(nick, body)
            if response:
                self._handle_response(response, False)
                break

    def _handle_response(self, response, admin):
        delay = self._timeout_delay()
        if delay and not admin:
            logging.debug('Timed out for %s secs', int(delay))
        else:
            self.send(response)
            if not admin:
                self._last_timeout = time.time()

    def _timeout_delay(self):
        if self.timeout:
            return max(0, self.timeout + self._last_timeout - time.time())

    def add_plugin(self, plugin):
        """Adds a FooPlug to the FooBot"""
        logging.info('Adding plugin %s', plugin)
        self._plugins.append(plugin)

    def send(self, text):
        """Sends a message to the chat room"""
        logging.debug("Sending: %s", text)
        self._client.send_message(mto=self._room,
                                  mbody=text,
                                  mtype='groupchat')

    def run(self):
        """Connects and runs the bot in blocking mode"""
        logging.debug('Connecting...')
        self._client.connect()
        logging.debug('Processing...')
        self._client.process(block=True)


def foocmd(func, name=None):
    """Marks a method or callable attribute as a FooBot command"""
    if name:
        func.__foocmd__ = name
    else:
        func.__foocmd__ = func.__name__.replace('_', ' ').strip()
    return func


def foocmd_admin(func, name=None):
    """Marks a method or callable attribute as an admin FooBot command"""
    func = foocmd(func, name)
    func.__fooadmin__ = True
    return func


def foocmd_mention(func, name=None):
    """Marks a method or callable attribute as a FooBot mention command"""
    func = foocmd(func, name)
    func.__foomention__ = True
    return func


class FooPlug(object):
    """Base class for FooBot plugins"""

    def handle_cmd(self, nick, msg, include_admin=False):
        """Runs the applicable foocmd (if any) on the given message"""
        return self._run_cmd(nick, msg, include_admin, False)

    def handle_mention(self, nick, msg):
        """Runs the applicable mention foocmd (if any) on the given message"""
        return self._run_cmd(nick, msg, False, True)

    def handle_help(self):
        """Generates a set of commands registered to the plugin"""
        cmds = self._foocmd_all(False, False)
        return {cmd.__foocmd__.split()[0] for cmd in cmds}

    def _run_cmd(self, nick, msg, include_admin, mention_cmd):
        cmd = self._foocmd_match(msg, include_admin, mention_cmd)
        if cmd:
            try:
                if not mention_cmd:
                    msg = msg[len(cmd.__foocmd__):]
                return cmd(nick, msg.strip())
            except Exception as exception: # pylint: disable=broad-except
                logging.error(exception)

    def register_command(self, name, callback):
        """Registers a new command with the FooPlug"""
        setattr(self, name, foocmd(callback, name))

    def register_admin_command(self, name, callback):
        """Registers a new admin command with the FooPlug"""
        setattr(self, name, foocmd_admin(callback, name))

    def register_mention_command(self, name, callback):
        """Registers a new mention command with the FooPlug"""
        setattr(self, name, foocmd_mention(callback, name))

    def _foocmd_all(self, include_admin, include_mention):
        for name in dir(self):
            cmd = getattr(self, name)
            if not hasattr(cmd, '__foocmd__'):
                continue
            if hasattr(cmd, '__fooadmin__') and not include_admin:
                continue
            if hasattr(cmd, '__foomention__') and not include_mention:
                continue
            yield cmd

    def _foocmd_matches(self, msg, include_admin, mention_cmd):
        for cmd in self._foocmd_all(include_admin, mention_cmd):
            if mention_cmd and hasattr(cmd, '__foomention__'):
                yield cmd
            elif not mention_cmd and msg.startswith(cmd.__foocmd__):
                yield cmd

    def _foocmd_match(self, msg, include_admin, mention_cmd):
        try:
            matches = self._foocmd_matches(msg, include_admin, mention_cmd)
            return max(matches, key=lambda c: len(c.__foocmd__))
        except (TypeError, ValueError):
            return None

    def __str__(self):
        return type(self).__name__
