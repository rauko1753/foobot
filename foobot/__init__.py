"""A simple chatbot intended for use on livecoding.tv"""

from .foobot import FooBot, FooPlug, foocmd, foocmd_admin, foocmd_mention
from .plugin import EightBall, Raffle, Poll, Decide, Rote, Greeter, Quotes
