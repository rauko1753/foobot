"""Example script on how to run FooBot"""

import foobot

class HelloWorld(foobot.FooPlug):
    """Example 'Hello, world!' plugin for FooBot."""

    @foobot.foocmd
    def hello(self, nick, msg):
        """Say hello to the world."""
        return 'Hello, world!'

bot = foobot.FooBot('<bot-username>@livecoding.tv',
                    '<bot-password>@livecoding.tv',
                    '<host-username>@chat.livecoding.tv')
bot.add_plugin(HelloWorld())
bot.run()
